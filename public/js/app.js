var iptv = angular.module("iptvApp", ["firebase", "ngRoute", "ngCookies"]);


iptv.factory("Auth", function($firebaseAuth) {
	var ref = new Firebase("https://iptvlist.firebaseio.com");
    return ref;
  }
);

iptv.factory("Admin", ["$firebaseArray",
  function($firebaseArray) {

    var fb = new Firebase("https://iptvlist.firebaseio.com").child("users");;
    var users = $firebaseArray(fb);
    
    return {
      all: function() {
        return users;
      },
      addnew: function() {
        return fb;
      }
    };
  }
])

iptv.factory("DB_ADMIN", ["$firebaseArray",
  function($firebaseArray) {

    var fb = new Firebase("https://iptvlist.firebaseio.com");
    var users = $firebaseArray(fb);
    
    return {
      get_all_from: function(child) {
        return $firebaseArray(fb.child(child));
      },
      add_new_to: function(child) {
        return fb.child(child);
      },
      remove_from: function(child) {
        return fb.child(child);
      },
      add_to_as: function(data) {
      	//var tmp = fb.child(data.join("/"));
        return fb.child(data.join("/"));
      },
      get_from: function(data) {
      	//var tmp = fb.child(data.join("/"));
        return $firebaseArray(fb.child(data.join("/")));
      },
      get_one_from: function(data) {
      	//var tmp = fb.child(data.join("/"));
        return fb.child(data.join("/"));
      }
    };
  }
]);

iptv.factory("Users", function($firebaseAuth) {
	var ref = new Firebase("https://iptvlist.firebaseio.com").child("users");
    return ref;
  }
);


iptv.controller('startCtrl', function ($scope, $cookies, $firebaseAuth, $location, $timeout, Auth, Users) {

  	$scope.name = "World";
  	$scope.loading = false;

  	auth = Auth;
   	user = Users;


	var authData = user.getAuth();

	if (authData) {
		//console.log("Logged in as:", authData.uid);
		$location.path("/user/"+ authData.uid);
	} else {
		//console.log("Logged out");
		$location.path("/");
	}

	$scope.authData = null;
    $scope.error = null;
    $scope.userLogin = function() {
    	$cookies.remove("is_admin");
    	$cookies.remove("user_id");
    	$scope.loading = true;

      	auth.authWithPassword({
		  	email: $scope.userEmail,
        	password: $scope.userPassword
		}).then(function(authData) {
			
			
			$timeout(function() {
				user.child(authData.uid).on("value", function(snapshot) {
					$timeout(function() {
				      var data = snapshot.val();
				      $cookies.put('user_id', authData.uid);				      
				      $scope.loading = false;
				      if(data.is_admin == 1){
				      	$cookies.put('is_admin', true);
				      	$location.path("/admin");	
				      }else {
				      	$location.path("/user/"+ authData.uid);	
				      }
			      	});
				}, function (errorObject) {
					console.log("The read failed: " + errorObject.code);
				});
		    });
        	
      	}).catch(function(error) {
        	$scope.error = error;
      	});


    };

});

iptv.controller('createCtrl', function ($scope, $cookies, $firebaseAuth, $location, Auth, Users) {

	$scope.name = "World";

	create = $firebaseAuth(Auth);
	user = Users;

    $scope.createUser = function() {
		$scope.message = null;
		$scope.error = null;

		create.$createUser({
			email: $scope.userEmail,
			password: $scope.userPassword
		}).then(function(userData) {

			user.child(userData.uid).set({
				user_name: $scope.userName,
				user_surname: $scope.userSurname,
				user_email: $scope.userEmail,
				user_tel: $scope.userTel,
				is_admin: $scope.userRole,
			});

			$location.path("/");

		}).catch(function(error) {
			$rootScope.error = error;
		});      
    };
});

iptv.controller('adminCtrl', function ($scope, $cookies, $location, Admin) {
	if(!$cookies.get('is_admin')) {
		$location.path("/");
	}

	//console.log(Boolean($cookies.get('is_admin')));
  	$scope.users = Admin.all();
});


iptv.controller('chanelsCtrl', function ($scope, $cookies, $location, DB_ADMIN) {
	// if(!$cookies.get('is_admin')) {
	// 	$location.path("/");
	// }
  // $scope.chanelsUser = this.chanelsUser;
  // var name = this.chanelsUser;
	
	// post.push({
	// 		    cid: "cid",
	// 		    name: "name",
	// 		    logo: "logo",
	// 		    url: "url"
	// 		});
	//console.log(post);
	$scope.uploadData = function(){
    var data = ['chanels',this.chanelsUser];
    post = DB_ADMIN.add_to_as(data);
    // console.log(data);
		var input = $scope.chanelsList;

		var IPTV_chanels = input.replace("#EXTM3U","")
							.replace(/tvg-id/gm,"#tvg-id")
							.replace(/\n#EXTINF:-1 /gm,"")
              .replace(/\n/gm,"*")
							//.replace(/ /gm,"")
							.split(/#/gm);

    for (var i = 1; i < IPTV_chanels.length; i++) {
      var tmp = IPTV_chanels[i].split(/\*/gm)[0]
              .replace(/group-title/gm,"tvg-group")
              .replace(/"/gm,"")
              .split(/tvg-/gm);

      var cid = parseInt(tmp[1].split(/=/gm)[1]);
      var name = tmp[2].split(/=/gm)[1];
      var logo = tmp[3].split(/=/gm)[1];
      var url = IPTV_chanels[i].split(/\*/gm)[1];

		
			post.push({
			    cid: cid,
			    name: name,
			    logo: logo,
			    url: url,
			    active: false,
		  		order: 9999
			});
      $location.path("/");
		};

		// console.log(input.replace("#EXTM3U","")
		// 					.replace(/tvg-id/gm,"#tvg-id")
		// 					.replace(/\n#EXTINF:-1 /gm,"")
		// 					.replace(/\n/gm,"*")
		// 					.replace(/ /gm,"")
		// 					.split(/#/gm)[1]
		// 					.split(/\*/gm)[0]
		// 					.replace(/group-title/gm,"tvg-group")
		// 					.replace(/"/gm,"")
		// 					.split(/tvg-/gm));
	};
	

	// post.push({
	//     cid: 47,
	//     name: "BHT",
	//     logo: "http://radiokafic.com/kanali-slike/bht.png"
	// });
	//console.log(Boolean($cookies.get('is_admin')));
  	//$scope.users = DB_ADMIN.get_all_from('chanels');
});


iptv.controller('epgCtrl', function ($scope, $routeParams, $timeout, $location, $cookies, Users, DB_ADMIN, Auth) {
  if(!$cookies.get("user_id")) {
    $location.path("/");
  }
  $scope.is_admin = $cookies.get("is_admin");
  $scope.data = null;
  $scope.chanels = null;
  $scope.epgs = DB_ADMIN.get_all_from('epg/chanels_content');
  $scope.active_chanels = null;
  $scope.inactive_chanels = null;
  $scope.loading = true;
  $scope.userName = "";
  $scope.listaEPG = {};
  

  user = Users;
  auth = Auth;

  $scope.logOut = function() {
    auth.unauth();
    $cookies.remove("is_admin");
    $cookies.remove("user_id");
    $location.path("/");
  };

  $scope.timeMargin = function(item, first) {

      if(first) {
        var current = item.split(":");
        var currentHur = parseFloat(parseInt(current[0]) + parseFloat((current[1]/60)));
        value = parseInt(currentHur*60) * 3;
        return value;
      }
  };

  $scope.timeWidth = function(item, next) {
      var current = item.split(":");

      var currentHur = parseFloat(parseInt(current[0]) + parseFloat((current[1]/60)));
      var value = 0;
      if( next != undefined) {
        var nextitem = next.split(":");
        var nextitemHour = parseFloat(parseInt(nextitem[0]) + parseFloat((nextitem[1]/60)));
        value = parseInt(parseFloat(nextitemHour - currentHur)*60) * 3;
      }else {
        value = 180;
      }

      return value;
  };

  $timeout(function() {
    user.child($routeParams.id).on("value", function(snapshot) {
      
      $timeout(function() {
          $scope.data = snapshot.val();
          $scope.userName = $scope.data.user_name;
          var data = ['chanels',$scope.userName.toLowerCase()];

          $scope.chanels = DB_ADMIN.get_from(data);
          
          });       

    });
  }, 500);

  

  

  var testArray = [];
  var obj = {};
  
  $timeout(function() {    
    for (var i = 0; i < $scope.chanels.length; i++) {

        if($scope.chanels[i]['epg']) {
          var epgID = $scope.chanels[i]['epg'];
          data = ['epg','chanels_content',epgID];
          epgdata = DB_ADMIN.get_one_from(data);
          
          epgdata.on("value", function(snapshot) {
              $timeout(function() {  
                  
                var epgsnap = snapshot.val();
                var snapKey = snapshot.key();
                
                //console.log(chanelData);

                 $scope.listaEPG[snapKey] = {
                  'chanel' : snapKey,
                  'epg' : epgsnap
                };
              });
          });
        }
        
    }

    var d = new Date(); 
    var currentHur = parseFloat(parseInt(d.getHours()) + parseFloat((d.getMinutes()/60)));
    var value = parseInt(currentHur*60) * 3;
    $('.epg_chanel_holder').css('margin-left',value);

    $('.current-time').css('left',value +80);
    $( ".epg_list_wrapper" ).scrollLeft( value - 100);
    
    console.log($('.real').length );
  },6000);

  $( ".epg_list_wrapper" ).scroll(function(e) {
      var leftMargin = $(this).scrollLeft();
      var topMargin = $(this).scrollTop();
      $('.epg_chanel_holder').css('margin-left',leftMargin);
  });
  

});

iptv.controller('userCtrl', function ($scope, $routeParams, $timeout, $location, $cookies, Users, DB_ADMIN, Auth) {
  if(!$cookies.get("user_id")) {
    $location.path("/");
  }
  $scope.is_admin = $cookies.get("is_admin");
  $scope.data = null;
  $scope.chanels = null;
  $scope.epgs = DB_ADMIN.get_all_from('epg/chanels_id');
  $scope.active_chanels = null;
  $scope.inactive_chanels = null;
  $scope.loading = true;
  $scope.userName = "";
  $scope.userEPG = $cookies.get("user_id");
  
  user = Users;
  auth = Auth;

  $scope.logOut = function() {
    auth.unauth();
    $cookies.remove("is_admin");
    $cookies.remove("user_id");
    $location.path("/");
  };

  $scope.chanelEdit = function(id){
  	$('#chanelSettings').modal('show');

  	data = ['chanels',$scope.userName.toLowerCase(), id]
  	edit = DB_ADMIN.get_one_from(data)
  	//console.log(edit);
  	edit.on("value", function(snapshot) {
    		$timeout(function() {
    			
  	     	var chanel = snapshot.val()
  	        $scope.chanel_id = id;
  	        $scope.chanel_logo = chanel.logo;
  	        $scope.chanel_name = chanel.name;
            $scope.chanel_url = chanel.url;
  	        $scope.chanel_epg_id = chanel.epg;
            $scope.chanel_epg_id_current = chanel.epg;
  	    });
  	}, function (errorObject) {
  	  console.log("The read failed: " + errorObject.code);
	   });
  };

  $scope.saveChanel = function(){
  	data = ['chanels',$scope.userName.toLowerCase(), $scope.chanel_id]
  	edit = DB_ADMIN.get_one_from(data)
  	edit.update({
	  "name": $scope.chanel_name,
	  "logo": $scope.chanel_logo,
    "url": $scope.chanel_url,
	  "epg": $scope.chanel_epg_id
	});
	$('#chanelSettings').modal('hide');
  	// console.log(
	  // 	$scope.chanel_logo,
	  //   $scope.chanel_name,
	  //   $scope.chanel_url
   //  );
  }

  $scope.deleteChanel = function(id){

    data = ['chanels',$scope.userName.toLowerCase(), $scope.chanel_id]
    edit = DB_ADMIN.get_one_from(data)
    edit.remove();

    $('#chanelSettings').modal('hide');
  }

  $scope.saveList = function(){
  	$scope.loading = true;
  	$("#your-list li").each(function(key){
        //console.log($(this).attr('data-cid'),parseInt(key));
        data = ['chanels',$scope.userName.toLowerCase(), $(this).attr('data-cid')]      
  		order = DB_ADMIN.get_one_from(data)
        order.update({
		  active: true,
		  order: key
		});
    });
    $("#chanel-list li").each(function(key){
        //console.log($(this).attr('data-cid'),parseInt(key));
        data = ['chanels',$scope.userName.toLowerCase(), $(this).attr('data-cid')]
  		order = DB_ADMIN.get_one_from(data)
        order.update({
		  active: false,
		  order: 9999
		});
    });
    var data = ['chanels',$scope.userName.toLowerCase()];
  	$scope.chanels = DB_ADMIN.get_from(data);
  	$timeout(function() {
		startOrder();
		$scope.loading = false;
    },6000);
  	// data = ['chanels',$scope.userName.toLowerCase(), $scope.chanel_id]
  	// edit = DB_ADMIN.get_one_from(data)

 //  	edit.update({
	//   "name": $scope.chanel_name,
	//   "logo": $scope.chanel_logo,
	//   "url": $scope.chanel_url
	// });
  }
	var startOrder = function(){
		$('.list-group-sortable-connected').sortable({
			placeholderClass: 'list-group-item',
			connectWith: '.connected'
	    }).bind('sortupdate', function(e, ui) {

            $("#chanel-list li").each(function(key){

                $(this).find('span.cnumb').text(parseInt(key + 1));
                //console.log($(this).attr('data-cid'),parseInt($(this).find('span.cnumb').text()));
            });
            $("#your-list li").each(function(key){
            	//console.log();
                $(this).find('span.cnumb').text(parseInt(key + 1));
                //console.log($(this).attr('data-cid'),parseInt($(this).find('span.cnumb').text()));
            });

        });
	}


	$timeout(function() {
		startOrder();
		$scope.loading = false;
    },6000);

	
  	
	
	user.child($routeParams.id).on("value", function(snapshot) {
		
		$timeout(function() {
	    	$scope.data = snapshot.val();
	    	$scope.userName = $scope.data.user_name;
	    	var data = ['chanels',$scope.userName.toLowerCase()];
  			$scope.chanels = DB_ADMIN.get_from(data);				
      	});      	

	}, function (errorObject) {
		console.log("The read failed: " + errorObject.code);
	});

	

  // $scope.$watch("chanels", function(newValue, oldValue) {
  // 	if ($scope.chanels) {
  // 	for (var i = 0; i < $scope.chanels.length; i++) {
  // 		status = $scope.chanels[i].active;
  // 		if(status) {
  // 			$scope.active_chanels.push($scope.chanels[i])
  // 			//console.log('desno', );
  // 		}else {
  // 			$scope.inactive_chanels.push($scope.chanels[i])

  // 			//console.log('lijevo', $scope.chanels[i]);
  // 		}
  		
  // 	};
    
  //     //$scope.greeting = "Greetings " + $scope.chanels;
      
  //   }
  // });
});

iptv.config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                controller: 'startCtrl',
                templateUrl: 'public/views/front.html'
            })
            .when('/user/:id', {
                controller: 'userCtrl',
                templateUrl: 'public/views/user.html'
            })
            .when('/admin', {
                controller: 'adminCtrl',
                templateUrl: 'public/views/admin.html'
            })
            .when('/create', {
                controller: 'createCtrl',
                templateUrl: 'public/views/create.html'
            })
            .when('/chanels', {
                controller: 'chanelsCtrl',
                templateUrl: 'public/views/chanels.html'
            }).when('/epg/:id', {
                controller: 'epgCtrl',
                templateUrl: 'public/views/epg.html'
            })
            /*.when('/content/add', {
                controller: 'ContentController',
                templateUrl: 'public/views/manage_content.html'
            }).when('/content/edit/:id', {
                controller: 'ContentController',
                templateUrl: 'public/views/manage_content.html'
            }).when('/category/', {
                controller: 'CategoryController',
                templateUrl: 'public/views/category.html'
            }).when('/cat/:cat_id', {
                controller: 'CategoryListCotroler',
                templateUrl: 'public/views/cat_list.html'
            }).when('/settings/', {
                controller: 'SettingsController',
                templateUrl: 'public/views/settings.html'
            }).when('/users/', {
                controller: 'UserController',
                templateUrl: 'public/views/users.html'
            })*/
            .otherwise({
              redirectTo: '/'
            });
       $locationProvider.html5Mode(false);
    });