$chanels = array(
"bht-1" => "BHT 1",
"federalna" => "FTV",
"rtrs" => "RTRS",
"pink-bh" => "Pink BH",
"obn-bosna" => "OBN",
"hayat" => "Hayat",
"alfa-tv" => "Alfa",
"tv-sarajevo" => "TV Sarajevo",
"tv-vogosca" => "TV Vogošća",
"al-jazeeira-balkans" => "Al Jazeeira Balkans",
"tv-kiss" => "TV Kiss",
"osm" => "OSM",
"behar-tv" => "Behar",
"tv-1" => "TV 1",
"atv" => "ATV",
"bn" => "BN",
"tv-istocno-sarajevo" => "TV Istočno Sarajevo",
"oscar-c" => "Oscar C",
"cmc" => "CMC",
"htv-1" => "HRT 1",
"htv-2" => "HRT 2",
"nova-tv" => "Nova TV",
"rts-sat" => "RTS sat",
"rtcg-sat" => "TV Crna Gora",
"slo-1" => "SLO 1",
"happy-tv" => "Happy",
"fox-life" => "Fox Life",
"fox-crime" => "Fox Crime",
"mgm" => "MGM",
"universal-channel" => "Universal Channel",
"tv-1000" => "TV1000",
"axn" => "AXN",
"scifi" => "SciFi",
"cinestar" => "CineStar TV",
"cinestar-action-thriller" => "CineStar Action Thriller",
"sportklub" => "Sport Klub",
"eurosport" => "Eurosport",
"eurosport-2" => "Eurosport 2",
"eurosportnews" => "EurosportNews",
"golf-klub" => "Golf Klub",
"espn-america" => "ESPN America",
"extreme-sports" => "Extreme Sports",
"sport-klub-prime" => "SK Prime",
"motors" => "Motors TV",
"fight-channel" => "Fight Channel",
"discovery" => "Discovery",
"viasat-explorer" => "Viasat Explorer",
"24kitchen" => "24kitchen",
"viasat-history" => "Viasat History",
"history-channel" => "History",
"animal-planet" => "Animal Planet",
"national-geographic" => "National Geographic",
"nat-geo-wild" => "Nat Geo Wild",
"viasat-nature" => "Viasat Nature",
"ci" => "Crime Investigation",
"discovery-id" => "Discovery ID",
"discovery-world" => "Discovery World",
"discovery-science" => "Discovery Science",
"travel-channel" => "Travel",
"disney" => "Disney",
"disney-xd" => "Disney XD",
"cartoon-network" => "Cartoon Network",
"jim-jam" => "Jim Jam",
"boomerang" => "Boomerang",
"ultra" => "Ultra",
"ginx-tv" => "Ginx TV",
"e%21-entertainment" => "E!",
"fashion-tv" => "Fashion TV",
"tlc" => "TLC",
"bbc-entertainment" => "BBC Entertainment",
"style" => "Style",
"mtv-adria" => "MTV",
"vh1" => "VH1",
"bn-music" => "BN Music",
"balkanika" => "Balkanika",
"dm-sat" => "DM SAT",
"pink-music" => "Pink Music",
"pink-extra" => "Pink Extra",
"pink-plus" => "Pink Plus",
"mtv-dance" => "MTV Dance",
"vh1-classic" => "VH1 Classic",
"mezzo" => "Mezzo",
"mtv-rocks" => "MTV Rocks",
"mtv-hits" => "MTV Hits",
"kcn" => "KCN",
"cnn" => "CNN",
"rai-uno" => "Rai Uno",
"rtl" => "RTL",
"tv-espana" => "TVE",
"tv-5-monde" => "TV 5 Monde",
"vox" => "VOX",
"trt-1" => "TRT 1",
"super-rtl" => "Super RTL",
"pro-7" => "PRO 7",
"dw" => "DW",
"kabel-1" => "Kabel 1",
"hbo" => "HBO",
"hbo-comedy" => "HBO Comedy",
"disovery-hd-showcase" => "Discovery HD Showcase",
"history-hd" => "History HD",
"national-geographic-hd" => "National Geographic HD",
"eurosport-hd" => "Eurosport HD",
"eurosport-2-hd" => "Eurosport 2 HD",
"fashion-tv-hd" => "Fashion TV HD",
"food-network-hd" => "Food Network HD",
"travel-hd" => "Travel HD",
"animal-planet-hd" => "Animal Planet HD",
"mtv-live-hd" => "MTV Live HD",
"espn-america-hd" => "ESPN America HD");